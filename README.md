# Roblox Scripts Main Hub

<details><summary>Driving / Emergency Response Games</summary>

[Link To Repo](https://gitlab.com/DevShubam/driving-roblox-scripts)

- [Liberty County 1](https://gitlab.com/DevShubam/driving-roblox-scripts/-/raw/main/libertycounty1.txt)
</details>

<details><summary>Shooters</summary>

[Link To Repo](https://gitlab.com/DevShubam/shooter-roblox)

- [Bad Buisness 1](https://gitlab.com/DevShubam/shooter-roblox/-/raw/main/bad-buisness-1.txt)

- [Strucid Monkey](https://gitlab.com/DevShubam/shooter-roblox/-/raw/main/strucidmonkey.txt)

- [OP Bruh Hub Arsenal](https://gitlab.com/DevShubam/shooter-roblox/-/raw/main/BRUH-HUB-arsenal.txt)
</details>

<details><summary>Anime</summary>
[Link to Repo](https://gitlab.com/DevShubam/anime-games/-/tree/main)

- [Your Bizzare Adventure](https://gitlab.com/DevShubam/anime-games/-/raw/main/Your-Bizzare-Adventure-1)

</details>


<details><summary>Lumber Tycoon</summary>

- [Auto Dupe 1](https://gitlab.com/DevShubam/tycoon-games-roblox/-/raw/main/Auto%20Money%20Dupe%20-%20Roblox.txt)

</details>
